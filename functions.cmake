# Major difference between function & macro is: a variable's scope is limited to
# the function while a macro's is limited to the scope the macro is used.
# Similar to typical C/C++ macros

include(CTest)

# Set default path variables
macro(set_default_paths)
  # Main source directories
  set(external_dir "${PROJECT_SOURCE_DIR}/external")
  set(header_dir "${PROJECT_SOURCE_DIR}/include")
  set(source_dir "${PROJECT_SOURCE_DIR}/src")

  # Conan source files
  set(conan_buildinfo_path "${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.cmake")

  # Build directories
  set(binary_dir "${PROJECT_BINARY_DIR}/bin")
  set(library_dir "${PROJECT_BINARY_DIR}/lib")
  set(static_dir "${PROJECT_BINARY_DIR}/lib/static")

  # Supplementary source directories
  set(examples_dir "${PROJECT_SOURCE_DIR}/examples")
  set(tests_dir "${PROJECT_SOURCE_DIR}/tests")
endmacro()

macro(set_default_options)
  # Export compilation database to project root, for use by YouCompleteMe &
  # language servers
  set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

  check_languages()
  set_default_paths()
endmacro()

function(set_target_defaults target)
  target_include_directories("${target}" PUBLIC "${header_dir}")
  target_link_libraries("${target}" "${build_dependencies}")
endfunction()

macro(check_build_mode)
  # Check build type, defaulting to release build
  string(REGEX MATCH
               "[Dd]ebug"
               build_debug
               "${CMAKE_BUILD_TYPE}")
  if(build_debug)
    message(STATUS "Debug build")

    set(_debug true)
  else()
    message(STATUS "Release build")
  endif()
endmacro()

function(build_defaults)
  check_build_mode()

  build_dir("${source_dir}")
  build_dir("${examples_dir}")

  if(_debug)
    build_dir("${tests_dir}")
  endif()
endfunction()

function(set_linker linker)
  # Set the desired linker: "/bin/ld.gold", "/bin/ld.lld"

  if(EXISTS "/bin/${linker}")
    set(CMAKE_{C,CXX}_LINK_EXECUTABLE "/bin/${linker}")
  endif()
endfunction()

macro(glob_c_sources src_dir)
  file(GLOB _build_sources "${src_dir}/*.c")
  list(APPEND build_sources "${_build_sources}")
endmacro()

macro(glob_cxx_sources src_dir)
  file(GLOB _build_sources "${src_dir}/*.cpp")
  list(APPEND build_sources "${_build_sources}")

  file(GLOB _build_sources "${src_dir}/*.cxx")
  list(APPEND build_sources "${_build_sources}")

  file(GLOB _build_sources "${src_dir}/*.cc")
  list(APPEND build_sources "${_build_sources}")
endmacro()

macro(set_c_build_flags target)
  if("${$ENV{CFLAGS}}")
    target_compile_options("${target}" PRIVATE "${$ENV{CFLAGS}}")
  else()
    target_compile_options("${target}" PRIVATE "-O2")
  endif()
endmacro()

macro(set_cxx_build_flags target)
  if("${$ENV{CXXFLAGS}}")
    target_compile_options("${target}" PRIVATE "${$ENV{CXXFLAGS}}")
  else()
    target_compile_options("${target}" PRIVATE "-O2")
  endif()
endmacro()

# Set compile flags: Debug/Release
macro(set_compile_flags target)
  if(_is_c OR _is_cxx)
    if(_debug)
      if("${$ENV{DEBUG_FLAGS}}")
        target_compile_options("${target}" PRIVATE "${$ENV{DEBUG_FLAGS}}")
      else()
        # Provide debugging information for GDB / system native if missing
        target_compile_options("${target}" PRIVATE "-ggdb")
      endif()
    else()
      if(_is_c)
        set_c_build_flags("${target}")
      elseif(_is_cxx)
        set_cxx_build_flags("${target}")
      endif()
    endif()
  endif()
endmacro()

macro(print_sys_info)
  if(NOT _sys_printed)
    message(STATUS "Operating System:   ${CMAKE_SYSTEM}")
    message(STATUS "System version:     ${CMAKE_SYSTEM_VERSION}")
  endif()

  message(STATUS "Compiler:           ${CMAKE_C_COMPILER}")
  message(STATUS "Current project:    ${PROJECT_NAME}")

  set(_sys_printed TRUE)
endmacro()

function(setup_target target)
  print_compile_flags("${target}")
endfunction()

function(print_compile_flags target)
  if(_is_c)
    message(STATUS "${target} compile flags:      ${CMAKE_C_FLAGS}")
  elseif(_is_cxx)
    message(STATUS "${target} compile flags:      ${CMAKE_CXX_FLAGS}")
  endif()
endfunction()

function(build_dir dir)
  if(EXISTS "${dir}/CMakeLists.txt")
    add_subdirectory("${dir}" "${dir}/build")
  endif()
endfunction()

macro(check_languages)
  get_property(_lang GLOBAL PROPERTY ENABLED_LANGUAGES)

  string(REGEX MATCH
               "[Cc]$"
               _is_c
               "${_lang}")
  string(REGEX MATCH
               "[Cc]([Xx])+$"
               _is_cxx
               "${_lang}")

  if(_is_c)
    # message(STATUS "C detected")
  endif()

  if(_is_cxx)
    # message(STATUS "C++ detected")

    unset(_is_c)
  endif()

endmacro()

macro(setup_default_tests)
  if(_is_c)
    glob_c_sources("${tests_dir}")
  elseif(_is_cxx)
    glob_cxx_sources("${tests_dir}")
  endif()

  foreach(_build_source ${build_sources})
    get_filename_component(_target "${_build_source}" NAME_WLE)
    add_executable("${_target}" "${_build_source}")

    set_compile_flags("${_target}")
    set_target_defaults("${_target}")
    target_link_libraries("${_target}" "${build_dependencies}")

    add_test("${_target}Test" "${_target}")
  endforeach()
endmacro()

macro(setup_default_examples)
  if(_is_c)
    glob_c_sources("${examples_dir}")
  elseif(_is_cxx)
    glob_cxx_sources("${examples_dir}")
  endif()

  foreach(_build_source ${build_sources})
    get_filename_component(_target "${_build_source}" NAME_WLE)
    add_executable("${_target}" "${_build_source}")

    set_compile_flags("${_target}")
    set_target_defaults("${_target}")
    target_link_libraries("${_target}" "${build_dependencies}")
  endforeach()
endmacro()

macro(setup_conan)
  message(STATUS "Binary directory:   ${PROJECT_BINARY_DIR}")

  if(NOT EXISTS "${conan_buildinfo_path}")
    execute_process(COMMAND conan "install" "."
                    WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
                    RESULT_VARIABLE cmd_return)
    message(STATUS "Command result:     ${cmd_return}")

    if(NOT EXISTS "${conan_buildinfo_path}")
      message(FATAL_ERROR "File not found:     \"${conan_buildinfo_path}\"")
    endif()
  endif()

  # Use conan build info
  include("${conan_buildinfo_path}")

  conan_basic_setup(TARGETS)
endmacro()

function(setup_conan_target target)
  foreach(_lib ${CONAN_LIBS_DEBUG})
    target_link_libraries("${target}" "${_lib}")
  endforeach()

  foreach(_lib ${CONAN_LIBS_RELEASE})
    target_link_libraries("${target}" "${_lib}")
  endforeach()

  print_compile_flags("${target}")
endfunction()

macro(check_enabled)
  if(${USE_FLATCC})
    # Define flags to use libs
    message(STATUS "Using Flatbuffers & flatcc")

    add_definitions(-DUSE_FLATCC=1)
    list(APPEND build_dependencies flatcc)
  endif()

  if(${USE_SCTP})
    message(STATUS "Using SCTP protocol library")

    add_definitions(-DUSE_SCTP=1)
  endif()

  if(${USE_WIRINGPI})
    message(STATUS "Using WiringPi")

    add_definitions(-DUSE_WIRINGPI=1)
    list(APPEND build_dependencies wiringPi)
  endif()
endmacro()
