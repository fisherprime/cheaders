# c_headers

My C library

## Requirements

1. [conan](https://conan.io/)
1. [flatcc](https://github.com/dvidelabs/flatcc)
1. [ninja](https://ninja-build.org/)
1. make

To disable the use of flatcc, comment out
`cmake.definitions["USE_FLATCC"] =1` from the conanfile.py; vector
serialization won't be ideal in this case.

## Building

### Dependency gathering

```sh
conan install .
```

### Building

```sh
conan build .
```

Build and package in one step

```sh
conan package .
```

## Notes

To build the flatbuffers1.11.0 package make sure you don't have
"-Wfloat-equal" in your "CXXFLAGS" (C++ compile flags). Flatbuffers has a
couple of "==" & "!=" float comparisons.

The project is configured to automatically generate the `compile_commands.json` file in the
functions.cmake file.
One can also execute the below code with the `compile_commands.json` export option:

```sh
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=YES .
```

Ninja doesn't have a test target like make, instead run:

```sh
ctest
```
