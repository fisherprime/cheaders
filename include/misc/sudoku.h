/**
 * @file sudoku.h
 * @brief C include file, Sudoku solving definitions
 * @author fisherprime
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _SUDOKU_H
#define _SUDOKU_H

// Necessary headers
// ////////////////////////////////
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum sudoku_return_codes {
// };
// ////////////////////////////////

/// Definitions
// ////////////////////////////////
#define ROW_LIMIT (size_t)9
#define COLUMN_LIMIT ROW_LIMIT
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Function prototypes: Sudoku operations
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

#endif // _SUDOKU_H
