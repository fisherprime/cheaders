/**
 * @file util.h
 * @brief C include file, utility include file
 * @author fisherprime
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _UTIL_H
#define _UTIL_H

/**
 * @note       using type sizes: int = 16bits, int8_t = 8bits, int16_t =
 *             16bits, int32_t = 32bits, int64_t = 64bits
 *
 * @note using "__extension__" to mark GCC extensions & suppress compile time warnings
 * @par calloc calloc arguments: 1st is number of elements & 2nd size of the elements, defaults to
 *sizeof the data type. Strings require an extra element for the NULL terminator. calloc (1, sizeof
 *(<type>) * <num_elements>) should be the same as calloc (<num_elements>, sizeof (<type>))
 *
 * @par snprintf specify size as string length + 1 for the NULL terminator.
 */

// Necessary headers
// ////////////////////////////////
#include <stdint.h>
#include <signal.h>
#include <stdio.h>

#include "types/vector/i32/i32_vector.h"

#if USE_SCTP == 1
#ifndef NDEBUG
#pragma message "SCTP enabled"
#endif

/**
 * @headerfile provide SCTP socket primitives, requires "-lsctp"
 *<netinet/sctp>
 */
#include <netinet/sctp.h>
#else
#ifndef NDEBUG
#pragma message "SCTP disabled"
#endif
#endif

// ////////////////////////////////

#if 0
struct hostent {
	char *h_name;                 /*  official name of host */
	char **h_aliases;             /*  alias list */
	int h_addrtype;               /*  host address type */
	int h_length;                 /*  length of address */
	char **h_addr_list;           /*  list of addresses */
}
#define h_addr h_addr_list[0] /*  for backward compatibility */
#endif

// Return statuses 0-100
// ////////////////////////////////
#define ERR_SIGABRT (int8_t) SIGABRT
#define ERR_SIGBUS (int8_t) SIGBUS
#define ERR_SIGILL (int8_t) SIGILL
#define ERR_SIGINT (int8_t) SIGINT
#define ERR_SIGSEGV (int8_t) SIGSEGV
#define ERR_SIGTERM (int8_t) SIGTERM

#define SUCCESS (int8_t)0

// Randomly assigned
enum gen_ret_code {
	ERR_GENERAL_FAILURE = (int8_t)-1,

	// Program execution
	ERR_INVALID_ARG, // Invalid argument
	ERR_INVALID_ARG_NO, // Invalid argument number
	ERR_LOOP_TIMEOUT, // Timeout for "infinite" while loops

	// Types
	ERR_NO_STRING // String doesn't exist

	/*     // Process ERR_CHILD_EXIT = (int8_t) 1, // Subprocess exit failure ERR_FORK,
                      // fork () failure ERR_TYPE_UNDEF,              // Process type not defined
 *
 *     // File stuff ERR_FD_CLEANUP,              // Failed to cleanup file descriptors
 *ERR_FTOK_CREATE,             // File token creation failure
 *
 *     // Pipes ERR_PIPE_CREATE,             // Pipe creation failure
 *
 *     // Shared memory ERR_SHMEM_ASSIGN,            // Shared memory pointer assign failure
 *ERR_SHMEM_CREATE,            // Shared memory creation failure ERR_SHMEM_DEL,               //
 * Shared memory deletion failure
 *
 *     // Message queue ERR_MSGQ_CREATE,             // Message queue creation failure ERR_MSGQ_DEL,
 *               // Message queue deletion failure
 *
 *     // Networking ERR_CONN_ESTABLISH,          // Failed to establish a connection
 *ERR_SERVER_CALL,             // Error occurred while calling RPC server ERR_SOCK_CREATE
 *             // socket creation failure */
};
// ////////////////////////////////

// Definitions
// ////////////////////////////////
// Using unallocated port range (49152 - 65535)
#define DEFAULT_ADDRESS_LEN (size_t)64
#define DEFAULT_BUFF_LEN (size_t)9216
#define DEFAULT_COMMAND_LENGTH (size_t)64
#define DEFAULT_DIGIT_LENGTH (size_t)4
#define DEFAULT_LOOP_LIMIT (size_t)300 // Iterations * 500,000ns
#define DEFAULT_PORT (uint16_t)51234
#define DEFAULT_PORT_BEGIN (uint16_t)50000
#define DEFAULT_PORT_END (uint16_t)65535
#define DEFAULT_STRING_LEN (size_t)128
#define HALF_STRING_LEN (size_t)32
#define HEX_STRING_LEN (size_t)16
#define STDIN (int8_t)0
#define STDOUT (int8_t)1
#define WISDOM                                                                 \
	"When you have learned to snatch the error code from the trap frame, it will be time for you to leave. -- Tao"

enum {
	UNDEFINED_PROCESS_ID = -1,
	CLIENT_PROCESS_ID = 0,
	SERVER_PROCESS_ID = 1
};

enum { NO = 0, YES = 1, TRUE = 1, FALSE = 0 };
// ////////////////////////////////

// Typedefs
// ////////////////////////////////

/**
 * @brief Process execution state
 */
typedef struct {
	__int8_t argc; // Argument count
	__int8_t process_type; // Process type
	__int8_t verbose; // Verbosity level
	char **argv; // Arguments
	struct sigaction *sigaction; // Signal handler
	volatile sig_atomic_t quit; // Termination check
	i32_vector *accept_sockets; // Accept networking sockets
	i32_vector *open_files; // Files
	i32_vector *std_sockets; // Standard networking sockets
} execution_state;
// ////////////////////////////////

// Commenting out C code
#if 0
#ifdef __cplusplus
if (false) {    // Begin of C code commented
#endif

#ifdef __cplusplus
} // End of C code commented
#endif
#endif

// Function prototypes: Utility operations
// ////////////////////////////////
// Signal utils
void _print_signal_handler_message(char *signal);
void signal_handler(int signal_id);

// Utility functions
__int8_t cleanup_files(void);
void delay(struct timespec *wait_time);
void hex_dump(char *description, void *address, int length, FILE *file_ptr);
void cleanup(void);
void setup(void);
void populate_usage_flags(char *flags, char *insertion_string);
void print_error(char *function, int level, char *message, ...);
void show_progress(__int8_t current_percentage, __int8_t max_percentage);

// String handling
void custom_snprintf(char *dest, size_t len, char *fmt_str, ...);
void default_custom_snprintf(char *dest, char *fmt_str, ...);
void custom_strncat(char *dest, char *src, size_t len);
void default_custom_strncat(char *dest, char *src);
void custom_strncpy(char *dest, char *src, size_t len);
void default_custom_strncpy(char *dest, char *src);

// Misc
void sudo(void);
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ISO C++ forbids braced-groups within expressions
#ifndef NDEBUG
#define LOGGING_FREE(ptr)                                                      \
	__extension__({                                                        \
		if (ptr) {                                                     \
			printf("[+] %s: freeing [%s]\n", __FUNCTION__, #ptr);  \
                                                                               \
			free(ptr);                                             \
			ptr = NULL;                                            \
		}                                                              \
	}) // End LOGGING_FREE
#else
#define LOGGING_FREE(ptr)                                                      \
	__extension__({                                                        \
		if (ptr) {                                                     \
			free(ptr);                                             \
			ptr = NULL;                                            \
		}                                                              \
	}) // End LOGGING_FREE
#endif

#define STRING_EXIST(str)                                                             \
	__extension__({                                                               \
		if (!str) {                                                           \
			print_error(                                                  \
				NULL, 0,                                              \
				(char *)"%s: Referenced string [%s] doesn't exist\n", \
				__FUNCTION__, #str);                                  \
			return ERR_NO_STRING;                                         \
		}                                                                     \
	}) // End STRING_EXIST

#define CLOSE_FD(fd, tmp, vec, pos)                                            \
	__extension__({                                                        \
		fd = *((int *)i32_vector_at(vec, pos));                        \
		print_error(NULL, 0,                                           \
			    (char *)"%s: Closing file descriptor [%d]\n",      \
			    __FUNCTION__, fd);                                 \
		close(fd);                                                     \
	}) // End CLOSE_FD

// Gone crazy with the brackets for safety
#define MAX(val_1, val_2) ((val_1) > (val_2) ? (val_1) : (val_2))

#define MIN(val_1, val_2) ((val_1) < (val_2) ? (val_1) : (val_2))

// Stringify a macro value
#define _STRINGIFY(exp_macro) #exp_macro // Stringify argument
#define STR(x) _STRINGIFY(macro) // Expand argument macro

// Get free string space (-1) for the null terminator
#define STRING_REM_DEF(str)                                                    \
	((sizeof(str) - strnlen((const char *)str, DEFAULT_STRING_LEN)) - 1)
#define STRING_REM_CUSTOM(str, len)                                            \
	((sizeof(str) - strnlen((const char *)str, len)) - 1)

// Get length of string with maxlen
#define STRING_LEN_DEF(str) (strnlen((const char *)str, DEFAULT_STRING_LEN))
#define STRING_LEN_CUSTOM(str, len) (strnlen((const char *)str, len))
// ////////////////////////////////

// Global variables
// ////////////////////////////////
extern execution_state g_state;
// ////////////////////////////////

// Other includes
// ////////////////////////////////

// ////////////////////////////////

#endif // _UTIL_H

/**
 * @par Cppcheck directives cppcheck-suppress memleak, cppcheck-suppress nullPointerRedundantCheck
 */
