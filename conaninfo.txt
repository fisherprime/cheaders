[settings]
    arch=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++11
    compiler.version=11
    os=Linux

[requires]
    flatbuffers/2.Y.Z

[options]


[full_settings]
    arch=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++11
    compiler.version=11
    os=Linux

[full_requires]
    flatbuffers/2.0.5:6557f18ca99c0b6a233f43db00e30efaa525e27e

[full_options]
    flatbuffers:fPIC=True
    flatbuffers:flatbuffers=deprecated
    flatbuffers:flatc=deprecated
    flatbuffers:header_only=False
    flatbuffers:options_from_context=deprecated
    flatbuffers:shared=False

[recipe_hash]


[env]

