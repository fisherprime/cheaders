#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "util.h"
#include "types/vector/i32/i32_vector.h"

/**
 * @brief      Access element with bounds checking
 *
 * @param      vec  The input vector
 * @param[in]  position     The position
 *
 * @return     pointer to the element at <position>
 */
int32_t *i32_vector_at(i32_vector *vec, size_t position)
{
	I32_VECTOR_EXIST(vec);

	// >= size limit
	if (position >= vec->limit)
		return (int32_t *)ERR_VECTOR_BOUNDS;

	return i32_vector_data(vec) + position;
} // End vector_at

/**
 * @brief      Access the last element
 *
 * @param      vec  The input vector
 *
 * @return     Pointer to the last element
 */
int32_t *i32_vector_rear(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	// Compensate for empty vectors
	return i32_vector_at(vec, (MAX(*i32_vector_size(vec), 1) - 1));
} // End vector_back

/**
 * @brief      Returns a pointer to underlying array (the storage)
 *
 * @param      vec  The input vector
 *
 * @return     pointer to underlying array
 */
int32_t *i32_vector_data(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	return vec->data;
} // End vector_data

/**
 * @brief      Access the 1st element
 *
 * @param      vec  The input vector
 *
 * @return     Pointer to the 1st element
 */
int32_t *i32_vector_front(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	return vec->data;
} // End vector_front

/**
 * @brief      Checks for an empty vector
 *
 * @param      vec  The input vector
 *
 * @return     0 = Not empty / 1 = Empty
 */
int8_t i32_vector_empty(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	// Vector is empty
	if (vec->size == (size_t)0)
		return YES;

	return NO;
} // End vector_empty

/**
 * @brief      Reserve array of <size>
 *
 * @param      vec  The input vector
 * @param[in]  new_size     The new size
 *
 * @return     Exit status
 */
int8_t i32_vector_reserve(i32_vector *vec, size_t new_size)
{
	I32_VECTOR_EXIST(vec);

	if (i32_vector_resize(vec, new_size)) {
		print_error(
			NULL, 2,
			(char *)"vector_reserve: \n\t\tError occurred while calling vector_resize");

		return ERR_VECTOR_RESERVE;
	}

	return SUCCESS;
} // End vector_reserve

/**
 * @brief      Shrink vector to fit data
 *
 * @param      vec  The input vector
 *
 * @return     Exit status
 */
int8_t i32_vector_shrink(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	if (i32_vector_resize(vec, SHRINK_VECTOR)) {
		print_error(
			NULL, 2,
			(char *)"vector_shrink_to_fit: \n\t\tError occurred while calling vector_resize");

		return ERR_VECTOR_SHRINK;
	}

	return SUCCESS;
} // End vector_shrink_to_fit

/**
 * @brief      Return pointer to vector's size limit
 *
 * @param      vec  The input vector
 *
 * @return     pointer to vector limit size
 */
size_t *i32_vector_limit(i32_vector *vec)
{
	VECTOR_EXIST_S(vec);

	return &vec->limit;
} // End vector_max_size

/**
 * @brief      Returns pointer to number of elements
 *
 * @param      vec  The input vector
 *
 * @return     pointer to number of elements
 */
size_t *i32_vector_size(i32_vector *vec)
{
	VECTOR_EXIST_S(vec);

	return &vec->size;
} // End vector_size

/**
 * @brief      Constructs a vector
 *
 * @param      vec  The input vector
 * @param[in]  vct_size     The vector size
 *
 * @return     Exit status
 */
int8_t i32_vector_init(i32_vector *vec, size_t vct_size)
{
	/**
	 * @note       ceil () returns the smallest integral value not less than argument It rounds off
	 *the result in a positive manner Returns integer not less than result Create initial vector of
	 *size multiples of 4
	 */
	size_t init_size = (size_t)(ceil((double)vct_size / 4) * 4);

	assert(init_size);

#ifndef DNDEBUG
	printf("[+] Vector size: %zu\n", init_size);
#endif

	vec->limit = init_size;
	vec->size = 0;
	vec->data = (int32_t *)calloc(init_size, sizeof(int32_t));

	// Performed by calloc
	/* memset(vec->data, 0, vec->vector_limit); */

	// Shouldn't proceed if calloc fails
	assert(vec->data);

	if (!vec->data) {
		print_error(
			(char *)"calloc", 0,
			(char *)"vector_init: \n\tThe vector->data region references non-existent memory");

		raise(SIGABRT);
	}

#ifndef DNDEBUG
	printf("[+] Vector initialized\n");
#endif

	return SUCCESS;
} // End vector_init

/**
 * @brief      Inserts an element
 *
 * @param      vec  The input vector
 * @param[in]  position     The position
 * @param[in]  user_input   The user input
 *
 * @return     Exit status
 */
int8_t i32_vector_insert(i32_vector *vec, size_t position, int32_t user_input)
{
	I32_VECTOR_EXIST(vec);

	int32_t *vec_start;

	size_t *vec_size = &vec->size;
	size_t *vec_lim = &vec->limit;

	// Initialize if un-initialized
	if ((*vec_lim == (size_t)0) || !vec->data)
		i32_vector_init(vec, DEFAULT_VECTOR_SIZE);

	if (i32_vector_at(vec, position) == (int32_t *)ERR_VECTOR_BOUNDS)
		i32_vector_resize(vec, EXPAND_VECTOR);

	vec_start = i32_vector_data(vec);

	if (!i32_vector_empty(vec) && (position < *vec_size - 1))
		// Create insertion slot
		for (size_t counter = (*vec_lim - 1); counter >= position;
		     counter--)
			*(vec_start + (counter + 1)) = *(vec_start + counter);

	*(vec_start + position) = user_input;
	*vec_size += 1;

	if (*i32_vector_at(vec, position) != user_input) {
		print_error(
			NULL, 0,
			(char *)"vector_insert: \n\tError occured while inserting");

		return ERR_VECTOR_INSERT;
	}

	return SUCCESS;
} // End vector_insert

/**
 * @brief      Inserts an element at the back
 *
 * @param      vec  The input vector
 * @param[in]  user_input   The user input
 *
 * @return     Exit status
 */
int8_t i32_vector_push_rear(i32_vector *vec, int32_t user_input)
{
	I32_VECTOR_EXIST(vec);

	if (i32_vector_insert(vec, vec->size, user_input) ||
	    (*i32_vector_rear(vec) != user_input))
		return ERR_VECTOR_PUSH;

	return SUCCESS;
} // End vector_push_back

/**
 * @brief      Erases the last element
 *
 * @param      vec  The input vector
 *
 * @return     Exit status
 */
int8_t i32_vector_cut_rear(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	if (i32_vector_empty(vec)) {
		print_error(NULL, 0,
			    (char *)"vector_pop_back: \n\tThe vector is empty");

		return ERR_VECTOR_EMPTY;
	}

	if (i32_vector_erase(vec, (int32_t)(vec->size - 1))) {
		print_error(
			NULL, 1,
			(char *)"vector_pop_back: \n\tError occurred while calling vector_erase");

		return ERR_VECTOR_ERASE;
	}

	return SUCCESS;
} // End vector_pop_back

/**
 * @brief      Remove first occurrence of specified input
 *
 * @param      vec  The input vector
 * @param[in]  user_input   The user input
 *
 * @return     Exit status
 */
int8_t i32_vector_cut_first(i32_vector *vec, int32_t user_input)
{
	I32_VECTOR_EXIST(vec);

	int32_t *vct_start;
	int32_t *vct_end;

	size_t counter = 0;

	if (i32_vector_empty(vec)) {
		print_error(
			NULL, 0,
			(char *)"vector_pop_front: \n\tThe vector is empty");

		return ERR_VECTOR_EMPTY;
	}

	vct_start = i32_vector_front(vec);
	vct_end = i32_vector_rear(vec);

	for (;
	     vct_start != (int32_t *)ERR_VECTOR_BOUNDS && vct_start <= vct_end;
	     counter++)
		if (*vct_start == user_input) {
			if (i32_vector_erase(vec, counter)) {
				print_error(
					NULL, 1,
					(char *)"vector_pop_front: \n\tError occured while calling vector_erase");

				return ERR_VECTOR_ERASE;
			}

			break;
		}

	return SUCCESS;
} // End vector_pop_front

/**
 * @brief      Remove last occurrence of specified input
 *
 * @param      vec  The input vector
 * @param[in]  user_input   The user input
 *
 * @return     Exit status
 */
int8_t i32_vector_cut_last(i32_vector *vec, int32_t user_input)
{
	I32_VECTOR_EXIST(vec);

	int32_t *vct_start;
	int32_t *vct_end;
	size_t counter = 0;

	if (i32_vector_empty(vec)) {
		print_error(NULL, 0,
			    (char *)"vector_pop_last: \n\tThe vector is empty");

		return ERR_VECTOR_EMPTY;
	}

	vct_start = i32_vector_rear(vec);
	vct_end = i32_vector_front(vec);
	counter = (*i32_vector_size(vec) - 1);

	for (;
	     vct_start != (int32_t *)ERR_VECTOR_BOUNDS && vct_start >= vct_end;
	     counter--)
		if (*vct_start == user_input) {
			if (i32_vector_erase(vec, counter)) {
				print_error(
					NULL, 1,
					(char *)"vector_pop_last: \n\tError occured while calling vector_erase");

				return ERR_VECTOR_ERASE;
			}

			break;
		} // End if

	return SUCCESS;
} // End vector_pop_last

/**
 * @brief      Erases an element
 *
 * @param      vec  The input vector
 * @param[in]  position     The position
 *
 * @return     Exit status
 */
int8_t i32_vector_erase(i32_vector *vec, size_t position)
{
	I32_VECTOR_EXIST(vec);

	int32_t *vct_start;
	int32_t *vct_end;

	if (i32_vector_empty(vec)) {
		print_error(NULL, 0,
			    (char *)"vector_erase: \n\tThe vector is empty");

		return ERR_VECTOR_EMPTY;
	}

	vct_start = i32_vector_at(vec, position);
	vct_end = i32_vector_rear(vec);

	for (;
	     vct_start != (int32_t *)ERR_VECTOR_BOUNDS && vct_start <= vct_end;
	     vct_start++)
		*vct_start = *(vct_start + 1);

	vec->size--;

	return SUCCESS;
} // End vector_delete

/**
 * @brief      Increment the size of the underlying array
 *
 * @param      vec  The input vector
 * @param[in]  new_size     The new size
 *
 * @return     Exit status
 */
int8_t i32_vector_resize(i32_vector *vec, int32_t new_size)
{
	I32_VECTOR_EXIST(vec);

	int32_t *reallocate;
	int32_t *vct_data = vec->data;

	size_t *vct_lim = &vec->limit;
	size_t *vct_size = &vec->size;

	switch (new_size) {
	case SHRINK_VECTOR:
		// Shrink to fit
		*vct_lim = *vct_size;

		break;
	case EXPAND_VECTOR:
		// Auto-adjust by doubling vector size
		*vct_lim = (size_t)(2 * *vct_lim);

		break;
	default:
		// Adjust to user-defined size
		*vct_lim = new_size;

		break;
	}

	reallocate = (int32_t *)realloc((int32_t *)vct_data, *vct_lim);

	/**
	 * Fails safely if multiplication overflows, relies on libbsd
	 */
	// int16_t *reallocate = (int16_t *) reallocarray ((void *) vct_data,
	// *vct_lim, sizeof (int16_t));

	if (!reallocate) {
		print_error(
			(char *)"realloc", 0,
			(char *)"vector_resize: \n\tReallocation failure, vector will be duplicated");

		if (i32_vector_copy(vec, &reallocate, *vct_lim)) {
			print_error(
				NULL, 1,
				(char *)"vector_resize: \n\tError occurred while calling vector_copy");

			return ERR_VECTOR_REALLOC;
		}

		LOGGING_FREE(vct_data);
	}

	vec->data = reallocate;

#ifndef DNDEBUG
	printf("[+] Vector resized\n"
	       "\t[New limit]: %zu\n",
	       *vct_lim);
#endif

	return SUCCESS;
} // End vector_resize

/**
 * @brief      Duplicates the element storage
 *
 * @param      vec  The input vector
 * @param      duplicate    The duplicate
 * @param[in]  new_size     The new size
 * @param[in]  num_blocks  The number blocks
 *
 * @return     Pointer to realloocated vector
 */
int8_t i32_vector_copy(const i32_vector *vec, int32_t **duplicate,
		       size_t new_size)
{
	I32_VECTOR_EXIST(vec);

	*duplicate = (int32_t *)calloc(new_size, sizeof(int32_t));

	/**
	 * memmove copies memory inlike the name suggests & is safer than memcpy should the src & dest
	 *memory areas overlap memmove(*duplicate, vec->data, vec->vector_size);
	 */
	if (!*duplicate) {
		print_error(
			(char *)"memmove", 2,
			(char *)"vector_copy: \n\t\tError occurred while duplicating the vector data");

		return ERR_VECTOR_COPY;
	}

	return SUCCESS;
} // End vector_copy

/**
 * @brief      Destroys a vector
 *
 * @param      vec  The input vector
 *
 * @return     Exit status
 */
int8_t i32_vector_free(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	vec->size = 0;
	vec->limit = 0;

	// Free the underlying array
	LOGGING_FREE(vec->data);
	// Free the heap vector struct memory
	LOGGING_FREE(vec);

	if (vec) {
		print_error(
			NULL, 2,
			(char *)"vector_free: \n\t\tError occurred while freeing the vector allocated memory");

		return ERR_VECTOR_LOGGING_FREE;
	}

	return SUCCESS;
} // End vector_free

/**
 * @brief      Clear a vector's element(s)
 *
 * @param      vec  The input vector
 *
 * @return     Exit status
 */
int8_t i32_vector_clear(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	if (i32_vector_empty(vec)) {
		print_error(NULL, 0,
			    (char *)"vector_clear: \nThe vector is empty");

		return ERR_VECTOR_EMPTY;
	}

	memset(vec->data, 0, *i32_vector_limit(vec));
	vec->size = 0;

#ifndef DNDEBUG
	printf("[+] Vector elements cleared\n");
#endif

	return SUCCESS;
} // End vector_clear

/**
 * @brief      Print vector details
 *
 * @param      vec  The input vector
 *
 * @return     Exit status
 */
int8_t i32_vector_details(i32_vector *vec)
{
	I32_VECTOR_EXIST(vec);

	int32_t *vct_start = i32_vector_data(vec);

	size_t *vct_size = i32_vector_size(vec);

	printf("[Details\n"
	       "\t[First]: %d\n"
	       "\t[Last]: %d\n"
	       "\t[Size]: %zu\n"
	       "\t[Limit]: %zu\n"
	       "\t[Vector]: [ ",
	       *vct_start, *i32_vector_rear(vec), *vct_size,
	       *i32_vector_limit(vec));

	for (size_t counter = 0; counter < *vct_size; counter++)
		printf("%" PRId32 " ", *(vct_start + counter));

	printf("]\n]\n");

	return SUCCESS;
} // End vector_details

/**
 * @brief      Set value at specified position
 *
 * @param      vec  The input vector
 * @param[in]  position     The position
 * @param[in]  user_input   The user input
 *
 * @return     Exit status
 */
int8_t i32_vector_set(i32_vector *vec, size_t position, int32_t user_input)
{
	I32_VECTOR_EXIST(vec);

	*i32_vector_at(vec, position) = user_input;

	return SUCCESS;
} // End vector_set

/**
 * @brief      Convert string to vector
 *
 * @param      user_string    The user string
 * @param      user_i_vector  The input vector from serialized string
 * @param[in]  len            The length of the inbound string
 *
 * @return     Exit status
 */
int8_t to_vector(char **user_string, i32_vector *user_i_vector, size_t len)
{
	STRING_EXIST(*user_string);

	hex_dump((char *)"Vector", *user_string, len, stdout);

#if USE_FLATCC == 1
	// Flatbuffers implementation
	size_t vct_size = 0;

	ns(Vector_table_t) vector;

	if (!(vector = ns(Vector_as_root(*user_string)))) {
		print_error(NULL, 0, "Vector_as_root: \n\tNo vector in buffer");

		return ERR_FLATCC_UNPACK;
	}

	assert(vector);

	vct_size = (size_t)ns(Vector_vector_size(vector));

	vector_init(user_i_vector, vct_size);
	memmove(user_i_vector->data, ns(Vector_data(vector)),
		vct_size * sizeof(int32_t));
	user_i_vector->vector_size = user_i_vector->vector_limit;
#else
	// Flatbuffers-independent implementation

	char *end_of_int;
	char *vct_lim_pos =
		(char *)(*user_string +
			 (STRING_LEN_DEF(*user_string) - DEFAULT_DIGIT_LENGTH));
	char *vct_size_pos =
		(char *)(*user_string + (STRING_LEN_DEF(*user_string) -
					 (DEFAULT_DIGIT_LENGTH * 2 + 1)));

	size_t vct_lim = (size_t)strtol(vct_lim_pos, &end_of_int, 10);
	size_t vct_size = (size_t)strtol(vct_size_pos, &end_of_int, 10);

#ifndef DNDEBUG
	printf("[+] To vector\n"
	       "\tstring: %s\n"
	       "\tstring_length: %zu\n"
	       "\tnum_elements: %zu\n"
	       "\tvector_size: %zu\n",
	       (char *)user_string, STRING_LEN_DEF(*user_string), vct_size,
	       vct_lim);
#endif

	assert(vct_size);
	assert(vct_lim >= vct_size);

	i32_vector_init(user_i_vector, vct_size);

	assert(i32_vector_size(user_i_vector));

	for (size_t iter = 0, pos = 0; iter < vct_size; iter++) {
		i32_vector_push_rear(
			user_i_vector,
			(int32_t)strtol((char *)(*user_string + pos),
					&end_of_int, 10));

		// <int> + <comma>
		pos += DEFAULT_DIGIT_LENGTH + 1;
	}
#endif // USE_FLATCC

	// Takes up lots of cycles to print
	i32_vector_details(user_i_vector);

	return SUCCESS;
} // End to_vector

/**
 * @brief      Convert vector to string
 *
 * @param      serialized_vector  The serialized vector
 * @param      vec      The input vector to serialize
 *
 * @return     Size of resultant string
 */
size_t to_string(i32_vector *vec, char **serialized_vec)
{
	I32_VECTOR_EXIST(vec);

	// Shrink before conversion
	// vector_shrink_to_fit (vec);

#if USE_FLATCC == 1
	// Flatbuffers implementation
	int32_t *temp;
	int32_t *vct_data = vector_data(vec);

	size_t vct_size = *vector_size(vec);
	size_t vct_lim = vct_size;
	size_t buffer_size = 0;

	flatcc_builder_t builder;
	flatcc_builder_t *vector_flat;

	vector_flat = &builder;
	// Initialize the builder object
	flatcc_builder_init(vector_flat);
	// Create a vector to hold the vector data
	flatbuffers_int32_vec_ref_t data;

	// Problematic function
	data = flatbuffers_int32_vec_create(vector_flat, vct_data, vct_size);
#ifndef DNDEBUG
	printf("[+] Flatbuffers vector created\n");
#endif

	ns(Vector_start_as_root(vector_flat));
	ns(Vector_data_add(vector_flat, data));
	ns(Vector_vector_size_add(vector_flat, vct_size));
	ns(Vector_vector_limit_add(vector_flat, vct_lim));
	// Complete the vector "object" & make it the buffer root object
	ns(Vector_end_as_root(vector_flat));

	/**
	 * @brief      Alternatively
	 *
	 * @brief      ns(Vector_create_as_root (vector_flat, data, vct_size, vcl_lim));
	 */

	/**
	 * @brief      Populate the buffer size variable & get a pointer to the generated buffer
	 */
	temp = (int32_t *)flatcc_builder_finalize_buffer(vector_flat,
							 &buffer_size);
	*serialized_vector =
		(char *)calloc(buffer_size + 1, sizeof(char) * sizeof(int32_t));
	assert(*serialized_vector);

	/**
	 * @brief      Alternatively
	 *
	 * @brief      flatcc_builder_get_direct_buffer (vector_flat,
	 *&buffer_size)
	 */

	// cppcheck-suppress pointerSize
	memmove(*serialized_vector, temp, buffer_size);
	assert(*serialized_vector);

	if (!ns(Vector_as_root(*serialized_vector))) {
		print_error(NULL, 0, "Vector_as_root: \n\tNo vector in buffer");

		return ERR_FLATCC_PACK;
	}

	// Cleanup
	flatcc_builder_clear(vector_flat);
	LOGGING_FREE(temp);
#else
	// Flatbuffers-independent implementation

	int32_t *vct_start;

	size_t *vct_lim;
	size_t *vct_size;
	size_t buffer_size = 0;
	size_t vector_dets_length = 0;
	size_t vector_string_length = 0;
	size_t vector_value_length = 0;

	char *vector_dets_string;
	char *vector_value_string;

	vector_value_length = DEFAULT_DIGIT_LENGTH + 1;
	vector_dets_length = vector_value_length * 2;
	vct_start = i32_vector_data(vec);
	vct_size = i32_vector_size(vec);
	vct_lim = vct_size;

	vector_string_length =
		(size_t)(*vct_size * vector_value_length + vector_dets_length);
	vector_dets_string =
		(char *)calloc(vector_dets_length + 1, sizeof(char));
	vector_value_string =
		(char *)calloc(vector_value_length + 1, sizeof(char));
	*serialized_vec =
		(char *)calloc(vector_string_length + 1, sizeof(char));

	assert(vector_dets_string && vector_value_string);
	assert(*serialized_vec);

#if 0
	if (!*serialized_vector || !vector_dets_string ||
	    !vector_value_string)
		print_error((char *) "calloc", 0, (char *) "Memory allocation failure");
#endif

	for (size_t iter = 0; iter < *vct_size; iter++) {
		snprintf((char *)vector_value_string, vector_value_length + 1,
			 "%04d,", *(vct_start + iter));
		default_custom_strncat((char *)*serialized_vec,
				       (char *)vector_value_string);
	}

	snprintf((char *)vector_dets_string, vector_dets_length, "%04d;%04d",
		 (int32_t)*vct_size, (int32_t)*vct_lim);
	default_custom_strncat((char *)*serialized_vec,
			       (char *)vector_dets_string);

#if 0
	// Write to string
	memcpy(serialized_vector, vector_data(&vec),
	       vector_size(&vec) * 8);

	if (snprintf(serialized_vector, string_length + 1, string_length,
	             "%d,%d,%d",
	             vec.data, vec.vector_size,
	             vec.vector_limit) > string_length + 1) {
		fprintf(stderr, "[!] Memory allocation failure\n");
		perror("\t[!] calloc");

		// fprintf (stderr, "%s() error: snprintf returned
		// truncated

		// result.\n", __func__);
		return NULL;
	}
#endif

	assert(STRING_LEN_DEF(*serialized_vec));
	buffer_size = STRING_LEN_DEF(*serialized_vec);

#ifndef NDEBUG
	printf("[+] To string\n"
	       "\tstring: %s\n"
	       "\tstring_length: %zu\n",
	       *serialized_vec, STRING_LEN_DEF(*serialized_vec));
#endif

	// Cleanup
	LOGGING_FREE(vector_dets_string);
	LOGGING_FREE(vector_value_string);
#endif // USE_FLATCC

	i32_vector_free(vec);

	hex_dump((char *)"Vector", *serialized_vec, buffer_size, stdout);

	return buffer_size;
} // End to_string
