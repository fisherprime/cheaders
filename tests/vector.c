/**
 * @file test.c
 * @brief Vector functions test
 * @author fisherprime
 * @version
 * @date 2019-09-02
 */

#undef NDEBUG
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>

#include "util.h"

int main(void)
{
	int32_t ret = 0;

	i32_vector *test_vec = (i32_vector *)calloc(1, sizeof(i32_vector));

	printf("[*] Running vector tests\n");

	ret = i32_vector_init(test_vec, 5);
	assert(ret == SUCCESS);
	printf("\n[PASS] Vector initialization\n");
	i32_vector_details(test_vec);

	ret = i32_vector_init(test_vec, 5);
	assert(ret == SUCCESS);
	printf("\n[PASS] Vector reinitialization\n");
	i32_vector_details(test_vec);

	ret = i32_vector_push_rear(test_vec, 5);
	assert(ret == SUCCESS);
	printf("\n[PASS] Vector push\n");
	i32_vector_details(test_vec);

	ret = i32_vector_cut_rear(test_vec);
	printf("\n[PASS] Vector pop last element\n");
	i32_vector_details(test_vec);

	// Fails --as expected--

	/* ret = i32_vector_cut_rear(test_vec);
	 * assert(ret==SUCCESS);
	 * printf("\n[PASS] Vector pop last element(safety)\n");
	 * i32_vector_details(test_vec); */

	return ret;
}
